use itertools::izip;
use ndarray::{array, Array2};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::ops::Mul;

pub type Matrix = Array2<f64>;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Activation {
    Sigmoid,
    ReLU,
    Linear,
}

#[derive(Serialize, Deserialize)]
pub enum CostFunction {
    MeanSquaredError,
    BinaryCrossEntropy,
}

#[derive(Serialize, Deserialize)]
pub struct Layer {
    units: usize,
    activation: Activation,
    weights: Matrix,
    biases: Matrix,
}

impl Layer {
    pub fn new(units: usize, activation: Activation) -> Self {
        Layer {
            units,
            activation,
            weights: array![[]],
            biases: array![[]],
        }
    }

    pub fn init(&mut self, num_inputs: usize, num_outputs: usize) {
        let mut rng = rand::thread_rng();
        const RANGE: std::ops::Range<f64> = -10e-5..10e-5;

        self.weights = Matrix::ones((num_inputs, num_outputs))
            .mapv(|_| rng.gen_range(RANGE));

        self.biases =
            Matrix::ones((1, num_outputs)).mapv(|_| rng.gen_range(RANGE));
    }
}

pub struct NumberOfInputs(pub usize);

pub struct Layers(pub Vec<Layer>);

#[derive(Serialize, Deserialize)]
pub struct NeuralNetwork {
    layers: Vec<Layer>,
    cost_function: CostFunction,
}

impl NeuralNetwork {
    /******************/
    /* INITIALIZATION */
    /******************/

    pub fn default() -> Self {
        NeuralNetwork {
            layers: Vec::default(),
            cost_function: CostFunction::MeanSquaredError,
        }
    }

    pub fn new(
        number_of_inputs: NumberOfInputs,
        layers: Layers,
        cost_function: CostFunction,
    ) -> Self {
        assert!(layers.0.len() >= 2);
        let mut model = NeuralNetwork {
            layers: layers.0,
            cost_function,
        };
        let first_layer = model.layers.first_mut().unwrap();
        first_layer.init(number_of_inputs.0, first_layer.units);
        model.init_layers();
        model
    }

    fn init_layers(&mut self) {
        for i in 1..self.layers.len() {
            let num_inputs = self.layers[i - 1].units;
            let num_outputs = self.layers[i].units;
            self.layers[i].init(num_inputs, num_outputs);
        }
    }

    pub fn number_of_inputs(&self) -> usize {
        self.layers.first().unwrap().weights.dim().0
    }

    pub fn number_of_outputs(&self) -> usize {
        self.layers.last().unwrap().weights.dim().1
    }

    /***********************/
    /* FORWARD PROPAGATION */
    /***********************/

    fn relu_single(x: f64) -> f64 {
        x.max(0.0)
    }

    fn relu(array: &Matrix) -> Matrix {
        array.mapv(NeuralNetwork::relu_single)
    }

    fn sigmoid_single(x: f64) -> f64 {
        use std::f64::consts::E;
        1.0 / (1.0 + E.powf(-x))
    }

    fn sigmoid(array: &Matrix) -> Matrix {
        array.mapv(NeuralNetwork::sigmoid_single)
    }

    pub fn propagate<T: Borrow<Matrix>>(
        &self,
        input: T,
    ) -> (Matrix, Vec<Matrix>, Vec<Matrix>) {
        let mut a = input.borrow().t().into_owned();
        let mut layer_outputs_z: Vec<Matrix> = Vec::new();
        let mut layer_outputs_a: Vec<Matrix> =
            vec![input.borrow().t().into_owned()];

        for layer in &self.layers {
            let weights = &layer.weights;
            let biases = &layer.biases;
            let z = a.dot(weights) + biases;
            let g = match layer.activation {
                Activation::ReLU => NeuralNetwork::relu(&z),
                Activation::Sigmoid => NeuralNetwork::sigmoid(&z),
                Activation::Linear => z.clone(),
            };
            layer_outputs_z.push(z);
            layer_outputs_a.push(g.clone());
            a = g;
        }

        (a, layer_outputs_a, layer_outputs_z)
    }

    /********/
    /* COST */
    /********/

    fn sigmoid_cost(y: f64, yp: f64) -> f64 {
        const EPSILON: f64 = 1e-15;
        if y == 1.0 {
            -(yp + EPSILON).ln()
        } else if y == 0.0 {
            (1.0 + EPSILON - yp).ln()
        } else {
            panic!("Invalid output for BinaryCrossEntropy: {}", y);
        }
    }

    pub fn cost(&mut self, inputs: &Vec<Matrix>, outputs: &Vec<Matrix>) -> f64 {
        let mut cost = 0.0_f64;
        for i in 0..inputs.len() {
            let (prediction, _, _) = self.propagate(&inputs[i]);
            let output = &outputs[i];
            match self.cost_function {
                CostFunction::MeanSquaredError => {
                    cost += (prediction - output).mapv(|x| x.powi(2)).sum();
                }
                CostFunction::BinaryCrossEntropy => {
                    let mut result = Matrix::zeros(output.raw_dim());
                    for (r, y, yp) in izip!(
                        result.iter_mut(),
                        output.iter(),
                        prediction.iter()
                    ) {
                        *r = NeuralNetwork::sigmoid_cost(*y, *yp);
                    }
                    cost += result.sum();
                }
            }
        }

        cost / outputs.len() as f64
    }

    /*******************/
    /* BACKPROPAGATION */
    /*******************/

    fn sigmoid_backward(z: &Matrix) -> Matrix {
        let sigmoid = z.mapv(NeuralNetwork::sigmoid_single);
        let diff = Matrix::ones(z.raw_dim()) - &sigmoid;
        sigmoid.mul(diff)
    }

    fn relu_backward_single(z: f64) -> f64 {
        if z >= 0.0 {
            1.0
        } else {
            0.0
        }
    }

    fn relu_backward(z: &Matrix) -> Matrix {
        z.mapv(NeuralNetwork::relu_backward_single)
    }

    fn linear_backward(z: &Matrix) -> Matrix {
        z.mapv(|_| 1.0)
    }

    fn prediction_error(&self, output: &Matrix, prediction: &Matrix) -> Matrix {
        match self.cost_function {
            CostFunction::MeanSquaredError => 2.0 * (prediction - output),
            CostFunction::BinaryCrossEntropy => {
                const OMEGA: f64 = 1e15;
                let ones = Matrix::ones(prediction.raw_dim());
                let adjusted_prediction =
                    (0.5 * &ones + OMEGA * prediction) / (OMEGA + 1.0);

                (&adjusted_prediction - output)
                    / (&adjusted_prediction * (&ones - &adjusted_prediction))
            }
        }
    }

    pub fn backpropagate_once<T: Borrow<Matrix>>(
        &mut self,
        input: T,
        output: T,
        alpha: f64,
    ) {
        let (prediction, layer_outputs_a, layer_outputs_z) =
            self.propagate(input);

        let delta_cost =
            self.prediction_error(&output.borrow().t().into_owned(), &prediction);

        let mut delta_a = delta_cost.t().into_owned();
        let last_layer_index = self.layers.len() - 1;

        let mut delta_weights: Vec<Matrix> = Vec::new();
        let mut delta_biases: Vec<Matrix> = Vec::new();

        for i in 0..self.layers.len() {
            let layer = &self.layers[i];
            delta_weights.push(Array2::zeros(layer.weights.raw_dim()));
            delta_biases.push(Array2::zeros(layer.biases.raw_dim()));
        }

        for i in (0..(self.layers.len())).rev() {
            let layers = &self.layers;
            let a_prev = &layer_outputs_a[i].t();
            let z = &layer_outputs_z[i].t().into_owned();

            let mut delta_a_z = match layers[i].activation {
                Activation::ReLU => NeuralNetwork::relu_backward(z),
                Activation::Linear => NeuralNetwork::linear_backward(z),
                Activation::Sigmoid => NeuralNetwork::sigmoid_backward(z),
            };

            if i != last_layer_index {
                let weights_next = &layers[i + 1].weights;
                // weights_next = delta_Zi+1_Ai
                // L'(...) = weights_next x L1(...)
                delta_a = weights_next.dot(&delta_a.t());
            }

            // g'(Zi) . L'(...)
            delta_a_z = (delta_a_z.mul(&delta_a)).t().into_owned();

            // a_prev = delta_Zi_Wi
            // weight => Ai x g'(Zi) x L'(...)
            // bias   =>  1 x g'(Zi) x L'(...)
            delta_weights[i] = a_prev.mul(&delta_a_z);
            delta_biases[i] = delta_a_z.clone();

            delta_a = delta_a_z;
        }

        // update weights and biases
        for i in 0..self.layers.len() {
            let layer = &mut self.layers[i];
            layer.weights = &layer.weights - &delta_weights[i] * alpha;
            layer.biases = &layer.biases - &delta_biases[i] * alpha;
        }
    }

    pub fn backpropagate<T: Borrow<Matrix>>(
        &mut self,
        inputs: &Vec<T>,
        outputs: &Vec<T>,
        alpha: f64,
    ) {
        for (input, output) in izip!(inputs, outputs) {
            self.backpropagate_once(input.borrow(), output.borrow(), alpha);
        }
    }

    /****************/
    /* READ SAMPLES */
    /****************/

    fn convert_values(values_string: &str) -> Matrix {
        let values_vector = values_string
            .split(' ')
            .filter_map(|s| s.parse::<f64>().ok())
            .collect::<Vec<f64>>();
        Array2::from_shape_vec((1, values_vector.len()), values_vector)
            .unwrap()
            .t()
            .into_owned()
    }

    pub fn read_samples(
        &self,
        file_name: &str,
    ) -> (Vec<Matrix>, Vec<Matrix>, Vec<String>) {
        let mut reader = BufReader::new(File::open(file_name).unwrap());
        let mut line = String::new();
        let mut input_samples = Vec::<Matrix>::new();
        let mut output_samples = Vec::<Matrix>::new();
        let mut labels = Vec::<String>::new();

        loop {
            line.clear();

            if reader.read_line(&mut line).unwrap() == 0 {
                break;
            }

            line.pop();
            let values_with_label = line.split('#').collect::<Vec<&str>>();

            if values_with_label.len() == 2 {
                labels.push(values_with_label[1].replace("  ", " "));
            } else if values_with_label.len() == 1 {
                labels.push(String::new());
            } else {
                panic!();
            }

            let values = values_with_label[0].split(':').collect::<Vec<&str>>();
            assert!(values.len() == 2);

            let inputs = NeuralNetwork::convert_values(values[0]);
            let outputs = NeuralNetwork::convert_values(values[1]);

            assert!(self.number_of_inputs() == inputs.len());
            assert!(self.number_of_outputs() == outputs.len());

            input_samples.push(inputs);
            output_samples.push(outputs);
        }

        (input_samples, output_samples, labels)
    }

    /*************/
    /* LOAD/SAVE */
    /*************/

    pub fn save_model(&self, file_name: &str) {
        let mut writer = BufWriter::new(File::create(file_name).unwrap());
        let json = serde_json::to_string(&self).unwrap();
        write!(writer, "{}", json).unwrap();
    }

    pub fn load_model(&mut self, file_name: &str) {
        let mut reader = BufReader::new(File::open(file_name).unwrap());
        let mut line = String::new();
        match reader.read_line(&mut line) {
            Ok(_) => {
                *self = serde_json::from_str::<NeuralNetwork>(&line).unwrap();
            }
            Err(e) => panic!("{}", e),
        }
    }
}
